(* ::Package:: *)

BeginPackage["waveguidelosses`"];

(*TotalAttenuationRate::usage = "Returns attenuation rate";*)
ConductorLoss::usage = "Conductor loss of CPW"
DielectricLoss::usage = "Dielectric loss of CPW"
Begin["`Private`"];
SetDirectory[NotebookDirectory[]];
Needs["waveguide`", "waveguide.m"]



(*Resistance[\[Rho], thickness_, waveguide_waveguide]:=UnitConvert[(\[Rho] GetLength[waveguide])/(GetS[waveguide]*thickness),"Ohms"]
ResistivityLoss
RadiationLoss*)
(*DielectricLoss*)



ConductorLoss[f_, waveguide_waveguide]:=Quantity[0.072 (QuantityMagnitude[f,"Gigahertz"])^2/(QuantityMagnitude[GetS[waveguide], "Micrometers"]*QuantityMagnitude[GetZ0[waveguide],"Ohms"])*(2QuantityMagnitude[GetLength[waveguide],"Micrometers"])^2,1/"Micrometers"]


DielectricLoss[waveguide_waveguide]:=27.3*(Get\[Epsilon]r[waveguide](Get\[Epsilon]eff[waveguide]-1)*0.001)/(Get\[Epsilon]eff[waveguide](Get\[Epsilon]r[waveguide]-1)*2GetLength[waveguide])


(*TotalAttenuationRate[frequency_, thickness_, waveguide_waveguide] = Module[{S,W,t, \[Epsilon]r, f, \[CapitalDelta]t, k, b, a},
S = QuantityMagnitude[GetS[waveguide],"Micrometers"];
W = QuantityMagnitude[GetW[waveguide],"Micrometers"];
f = QuantityMagnitude[frequency, "Gigahertz"];
t = QuantityMagnitude[thickness,"Micrometers"];
\[Epsilon]r = Get\[Epsilon]r[waveguide];

\[CapitalDelta]t = (1.25t)/\[Pi] (1+Log[(4\[Pi] S)/t]);
k = (S+\[CapitalDelta]t)/(S+2W-\[CapitalDelta]t);
b = 0.183(t+0.464)-0.095k^2.484 (t - 2.595);
a=Sqrt[(\[Epsilon]r+1)/2](45.152/((S W)E^(2.125Sqrt[t])));
Quantity[a*f^b,"Decibels"/"Meters"]
]*)


End[]
EndPackage[]




