(* ::Package:: *)

BeginPackage["waveguide`"]
waveguide::usage = "Represents a waveguide"
ConstructWaveguide::usage = "Returns a waveguide object"
GetS::usage = "Returns waveguide central resonator width"
GetW::usage = "Returns waveguide air gap width"
Geth::usage = "Returns waveguide baseplate depth"
GetLength::usage = "Returns waveguide length"
Get\[Epsilon]r::usage = "Returns relative permetivity of base plate"
GetZ0::usage = "Returns waveguide impedance"
GetCCPW::usage = "Returns (something)"
Get\[Epsilon]eff::usage = "effective \[Epsilon]"

EffectiveCapacitance::usage = "Returns effective capacitance of waveguide"
ZeroPointVoltage::usage = "Returns the zero point volage of resonator"

DrawWaveGuide::usage = "Draws 3D image of waveguide"
DrawWaveGuide2d::usage = "Draws a 2D front-on image of waveguide"
Begin["`Private`"]

KEllipRatio[k_]:=EllipticK[k^2]/EllipticK[1-k^2]

ConstructWaveguide[S_, W_,h1_,length_, \[Epsilon]r_ ]:=Module[{Sunits,Wunits, h1units, lengthunits, c, \[Epsilon]0,k0,k1,CCPW,CAir,\[Epsilon]eff,Z0},
Sunits = Quantity[S, "Micrometers"];
Wunits = Quantity[W, "Micrometers"];
h1units = Quantity[h1, "Micrometers"];
lengthunits = Quantity[length, "Micrometers"];
c = Quantity[3*10^8, "Meters"/"Seconds"];
\[Epsilon]0 = Quantity[8.85*10^-12, "Farads"/"Meters"];
k0=1/(1+2 W/S);
k1 = Sinh[\[Pi]/(4 h1/S)]/Sinh[(\[Pi]*(1+2 W/S))/(4 h1/S)];
CAir=4\[Epsilon]0 *KEllipRatio[k0];
CCPW=2\[Epsilon]0 (\[Epsilon]r-1)KEllipRatio[k1]+4\[Epsilon]0 KEllipRatio[k0];
\[Epsilon]eff = CCPW/CAir;
Z0 =UnitConvert[ 1/(c CAir Sqrt[\[Epsilon]eff]), "Ohms"];
waveguide[{Sunits,Wunits, h1units,lengthunits, \[Epsilon]r},{CCPW,CAir,\[Epsilon]eff,Z0}]
]

GetS[waveguide_waveguide]:=waveguide[[1,1]]
GetW[waveguide_waveguide]:=waveguide[[1,2]]
Geth[waveguide_waveguide]:=waveguide[[1,3]]
GetLength[waveguide_waveguide]:=waveguide[[1,4]]
Get\[Epsilon]r[waveguide_waveguide]:=waveguide[[1,5]]
GetZ0[waveguide_waveguide]:= waveguide[[2,4]]
GetCCPW[waveguide_waveguide]:= waveguide[[2,1]]
Get\[Epsilon]eff[waveguide_waveguide]:= waveguide[[2,3]]


EffectiveCapacitance[waveguide_waveguide, \[Omega]_]:= \[Pi]/(2\[Omega] GetZ0[waveguide] )
ZeroPointVoltage [waveguide_waveguide]:= Sqrt[(\[HBar] \[Omega])/(2EffectiveCapacitance[waveguide])]


(*3D representation of waveguide*)
DrawWaveGuide[waveguide_]:=Module[{a,b,bound,h, baseplate, resonator, leftplate, rightplate},
a = QuantityMagnitude[GetS[waveguide]/2];
b = QuantityMagnitude[GetW[waveguide]];
bound= QuantityMagnitude[1.5*(GetS[waveguide]+GetW[waveguide])];
h = QuantityMagnitude[Geth[waveguide]];
baseplate = Cuboid[{-bound,-bound,-h},{bound,bound,0}];
resonator = Cuboid[{-a,-bound,0},{a,bound,0.1}];
leftplate = Cuboid[{-bound,-bound,0},{-(a+b),bound,0.1}];
rightplate = Cuboid[{(a+b),-bound,0},{bound,bound,0.1}];
Graphics3D[{Green,Opacity[.3],baseplate,Yellow,Opacity[1],resonator,leftplate,rightplate}, ImageSize->Medium]
]


DrawWaveGuide2d[waveguide_, bound_]:=Module[{a,b,h,baseplate, resonator, leftplate, rightplate},
a = QuantityMagnitude[GetS[waveguide]/2];
b = QuantityMagnitude[GetW[waveguide]];
h = QuantityMagnitude[Geth[waveguide]];
baseplate = Rectangle[{-bound,-h},{bound,0}];
resonator = Rectangle[{-a,-0.05},{a,0.05}];
leftplate = Rectangle[{-Max[bound,(a+b)],-0.05},{-(a+b),0.05}];
rightplate = Rectangle[{(a+b),-0.05},{Max[bound,(a+b)],0.05}];
Graphics[{Green,Opacity[0.3],baseplate,Yellow,Opacity[1],resonator,leftplate,rightplate}]
]


End[]
EndPackage[]




