(* ::Package:: *)

BeginPackage["waveguideEField`"];


airEField::usage = "Returns the electric field in air at the given point for the given waveguide";
substrateEField::usage = "Returns the electric field in the substrate at the given point for the given waveguide";
totalEField::usage = "Returns the electric field at the given point for the given waveguide";


DisplayEField::usage = "Plots Electric field superposed on diagram of waveguide"


fieldDecayLengthScale::usage = "Gives the longest decay scale of the electric field"


airEFieldModSquared::usage = "Returns E field norm squared at the given point"
airEFieldMod::usage = "Returns E field norm at the given point"


Begin["`Private`"]
Needs["waveguide`"]


guide\[Lambda][a_] := 2a
\[Lambda]Ratio[\[Lambda]_,a_]:=\[Lambda]/guide\[Lambda][a]
guide\[Lambda][\[Lambda]_,waveguide_waveguide] :=guide\[Lambda][GetLength[waveguide]]
\[Lambda]Ratio[\[Lambda]_,waveguide_waveguide]:=\[Lambda]Ratio[\[Lambda],GetLength[waveguide]]
FirstHarmonicWavelength[waveguide_waveguide]:=2GetLength[waveguide]
FirstHarmonicFrequency[waveguide_waveguide]:= (2\[Pi] c)/FirstHarmonicWavelength[waveguide]

Getb[waveguide_waveguide]:=1000*(GetS[waveguide]/2+GetW[waveguide])
Get\[Delta][waveguide_waveguide]:=GetW[waveguide]/Getb[waveguide]
Getd[waveguide_waveguide]:=(GetW[waveguide]+GetS[waveguide])/Getb[waveguide]


(*Air Electric Field Helper Functions*)
\[Nu][\[Lambda]_,a_]:=Sqrt[(\[Lambda]Ratio[\[Lambda],a])^2-1]
Fn[b_,\[Lambda]_,a_, n_]:=Sqrt[1+((2 b \[Nu][\[Lambda],a])/(n \[Lambda]))^2]
\[Gamma][b_,\[Lambda]_,a_, n_] :=(n \[Pi])/b Fn[b,\[Lambda],a, n]
 
\[Nu][\[Lambda]_,waveguide_waveguide]:=\[Nu][\[Lambda], GetLength[waveguide]]
Fn[\[Lambda]_,n_, waveguide_waveguide]:=Fn[Getb[waveguide],\[Lambda],GetLength[waveguide], n]
\[Gamma][\[Lambda]_, n_, waveguide_waveguide] :=\[Gamma][Getb[waveguide],\[Lambda],GetLength[waveguide], n]
fieldDecayLengthScale[\[Lambda]_, waveguide_waveguide,n_:1] := 1/\[Gamma][\[Lambda], n, waveguide]


(*Substrate Electric Field Helper Functions*)
u[\[Lambda]_, a_,\[Epsilon]r_]:=Sqrt[\[Epsilon]r -(\[Lambda]Ratio[\[Lambda],a])^2]
Fn1[b_,\[Lambda]_,a_,n_, \[Epsilon]r_]:=Sqrt[1-((2 b u[\[Lambda],a,\[Epsilon]r])/(n \[Lambda]))^2]
\[Gamma]1[b_,\[Lambda]_,a_, n_, \[Epsilon]r_] :=(n \[Pi])/b Fn1[b,\[Lambda],a, n, \[Epsilon]r]
rn[b_,\[Lambda]_,a_, n_, \[Epsilon]r_, h_]:= \[Gamma]1[b,\[Lambda],a, n, \[Epsilon]r]h+ArcTanh[Fn1[b,\[Lambda],a,n, \[Epsilon]r]/(\[Epsilon]r Fn[b,\[Lambda],a, n])] 
qn[b_,\[Lambda]_,a_, n_, \[Epsilon]r_, h_]:= \[Gamma]1[b,\[Lambda],a, n, \[Epsilon]r]h+ArcCoth[Fn1[b,\[Lambda],a,n, \[Epsilon]r]/(\[Epsilon]r Fn[b,\[Lambda],a,n])] 
u[\[Lambda]_, waveguide_waveguide]:=u[\[Lambda], GetLength[waveguide], Get\[Epsilon]r[waveguide]]
Fn1[\[Lambda]_,n_, waveguide_waveguide]:=Fn1[Getb[waveguide],\[Lambda],GetLength[waveguide], n,Get\[Epsilon]r[waveguide]]
\[Gamma]1[\[Lambda]_, n_, waveguide_] :=\[Gamma]1[Getb[waveguide],\[Lambda],GetLength[waveguide], n,Get\[Epsilon]r[waveguide]]


(*Space savers*)
SincSinSin[n_, \[Delta]_, d_,b_,y_]:=Sinc[(n \[Pi] \[Delta])/2]Sin[(n \[Pi] d)/2]Sin[(n \[Pi] y)/b] 
SincSinCos[n_, \[Delta]_, d_,b_,y_]:=Sinc[(n \[Pi] \[Delta])/2]Sin[(n \[Pi] d)/2]Cos[(n \[Pi] y)/b] 


(*Air Electric Field*)
airEFieldy[y_,z_, \[Lambda]_,a_,V0_, b_, \[Delta]_,d_, nmax_:1000]:=-((2V0)/b)\!\(
\*SubsuperscriptBox[\(\[Sum]\), \(n = 1\), \(nmax\)]\(SincSinSin[n, \ \[Delta], \ d, b, y]\ 
\*SuperscriptBox[\(E\), \(\(-\[Gamma][b, \[Lambda], a, \ n]\) Abs[z]\)]\)\)
airEFieldz[y_,z_, \[Lambda]_,a_,V0_, b_, \[Delta]_,d_, nmax_:1000]:=-((2V0)/b)\!\(
\*SubsuperscriptBox[\(\[Sum]\), \(n = 1\), \(nmax\)]\(
\*FractionBox[\(SincSinCos[n, \ \[Delta], \ d, b, y]\), \(Fn[b, \[Lambda], a, \ n]\)] 
\*SuperscriptBox[\(E\), \(\(-\[Gamma][b, \[Lambda], a, \ n]\) Abs[z]\)]\)\)
airEFieldy[y_,z_, \[Lambda]_,V0_, waveguide_waveguide, nmax_:1000]:=airEFieldy[y,z, \[Lambda],GetLength[waveguide],V0, Getb[waveguide], Get\[Delta][waveguide],Getd[waveguide],nmax]
airEFieldz[y_,z_, \[Lambda]_,V0_, waveguide_waveguide, nmax_:1000]:=airEFieldz[y,z, \[Lambda],GetLength[waveguide],V0, Getb[waveguide], Get\[Delta][waveguide],Getd[waveguide],nmax]

airEField[y_,z_, \[Lambda]_,a_,V0_, b_, \[Delta]_,d_, nmax_:1000]:= (# @@ {y,z, \[Lambda],a,V0, b, \[Delta],d, nmax}) &/@ {airEFieldy, airEFieldz}
airEField[y_,z_, \[Lambda]_,V0_, waveguide_waveguide, nmax_:1000]:= airEField[y,z,\[Lambda],GetLength[waveguide],V0,Getb[waveguide], Get\[Delta][waveguide], Getd[waveguide],nmax ]


(*Substrate Electric Field*)
substrateEFieldx[y_,z_, \[Lambda]_,a_,V0_, b_, \[Delta]_,d_, h_, \[Epsilon]r_, nmax_:1000]:=j (2V0)/guide\[Lambda][a] \!\(
\*SubsuperscriptBox[\(\[Sum]\), \(n = 1\), \(nmax\)]\(
\*FractionBox[\(2\), \(n \((1 + 
\*SuperscriptBox[\((
\*FractionBox[\(2  b\), \(n\ guide\[Lambda][a]\)])\), \(2\)])\)\)] SincSinCos[n, \ \[Delta], \ d, b, y]\ \((Coth[qn[b, \[Lambda], a, \ n, \ \[Epsilon]r, \ h]] - Tanh[rn[b, \[Lambda], a, \ n, \ \[Epsilon]r, \ h]])\) Sinh[\[Gamma]1[b, \[Lambda], a, \ n, \ \[Epsilon]r]\ Abs[z]]\)\)
substrateEFieldy[y_,z_, \[Lambda]_,a_,V0_, b_, \[Delta]_,d_, h_, \[Epsilon]r_,nmax_:1000]:=-((2V0)/b)\!\(
\*SubsuperscriptBox[\(\[Sum]\), \(n = 1\), \(nmax\)]\(SincSinSin[n, \ \[Delta], \ d, b, y] \((\ Cosh[\[Gamma]1[b, \[Lambda], a, \ n, \ \[Epsilon]r]\ z] - \((
\*FractionBox[\(Tanh[rn[b, \[Lambda], a, \ n, \ \[Epsilon]r, \ h]] + 
\*SuperscriptBox[\((
\*FractionBox[\(2  b\), \(n\ guide\[Lambda][a]\)])\), \(2\)] Coth[qn[b, \[Lambda], a, \ n, \ \[Epsilon]r, \ h]]\), \(1 + 
\*SuperscriptBox[\((
\*FractionBox[\(2  b\), \(guide\[Lambda][a]\)])\), \(2\)]\)])\) Sinh[\[Gamma]1[b, \[Lambda], a, \ n, \ \[Epsilon]r]\ Abs[z]])\)\)\)
substrateEFieldz[y_,z_, \[Lambda]_,a_,V0_, b_, \[Delta]_,d_, h_, \[Epsilon]r_,nmax_:1000]:=-((2V0)/b)\!\(
\*SubsuperscriptBox[\(\[Sum]\), \(n = 1\), \(nmax\)]\(SincSinCos[n, \ \[Delta], \ d, b, y] \((\ Sinh[\[Gamma]1[b, \[Lambda], a, \ n, \ \[Epsilon]r]\ z] - Tanh[rn[b, \[Lambda], a, \ n, \ \[Epsilon]r, \ h]] Cosh[\[Gamma]1[b, \[Lambda], a, \ n, \ \[Epsilon]r] Abs[z]])\)\)\)

substrateEFieldy[y_,z_, \[Lambda]_,V0_, waveguide_waveguide]:=substrateEFieldy[y,z, \[Lambda],GetLength[waveguide],V0, Getb[waveguide], Get\[Delta][waveguide],Getd[waveguide], Geth[waveguide], Get\[Epsilon]r[waveguide]]
substrateEField[y_,z_, \[Lambda]_,a_,V0_, b_, \[Delta]_,d_, h_, \[Epsilon]r_,nmax_:1000]:= (# @@ {y,z, \[Lambda],a,V0, b, \[Delta],d, h, \[Epsilon]r,nmax}) &/@ {substrateEFieldx,substrateEFieldy, substrateEFieldz}


totalEField[y_,z_, \[Lambda]_,V0_, waveguide_waveguide]:=HeavisideTheta[z]airEField[Quantity[y, "Micrometers"],Quantity[z, "Micrometers"], Quantity[\[Lambda], "Micrometers"],Quantity[V0, "Volts"], waveguide]+ HeavisideTheta[-z]substrateEField[Quantity[y, "Micrometers"],-Quantity[z, "Micrometers"], Quantity[\[Lambda], "Micrometers"],Quantity[V0, "Volts"], waveguide][[{2,3}]]


(*Plots Electric field superposed on diagram of waveguide*)
DisplayEField[waveguide_waveguide, wavelength_, range_]:=Module[{rangeunits,fieldPlot},
rangeunits = Quantity[range, "Micrometers"];
fieldPlot=VectorPlot[QuantityMagnitude[Re[totalEField[y,z,wavelength,1,waveguide]], "Volts"/"Micrometers"],{y,-range,range},{z,-QuantityMagnitude[Geth[waveguide]],5}, VectorPoints->Fine, VectorScale->Medium];
Show[fieldPlot,DrawWaveGuide2d[waveguide,range], PlotRange->{{-range,range},Automatic}]
]


airEFieldModSquared[y_,z_,waveguide_waveguide,\[Lambda]_, V0_]:=Plus@@((Re[#])^2&/@airEField[y,z,\[Lambda],V0,waveguide] )
airEFieldMod[y_,z_,waveguide_waveguide,\[Lambda]_, V0_]:=Sqrt[airEFieldModSquared[y,z,waveguide,\[Lambda], V0]]


End[]
EndPackage[]

