(* ::Package:: *)

BeginPackage["waveguideCQED`"];


PhotonEnergy::usage = "Gives energy of photon of given wavelength"
CylinderCavity::usage = "Gives volume of resonator cavity"
PhotonElectricField::usage = "Returns electric field of photon in cavity"
RabiFrequency::usage = "Returns the Rabi frequency of a CaF molecule in the cavity"


CasimirPolder::usage = "Calculates the Casimir-Polder force for a CaF molecule above a surface"


Begin["`Private`"];
Needs["waveguide`"];


\[HBar] =Quantity[ (6.63*10^-34)/(2 \[Pi] ),"Joules"* "Seconds" ];
c = Quantity[3*10^8, "Meters"/"Seconds"];
\[Epsilon]0 = Quantity[8.85*10^-12, "Farads"/"Meters"];
ElectricDipoleMoment=Quantity[3.06/(2.9979*10^(29)) , "Coulombs"*"Meters"];
kb = Quantity[1.38*10^-23, "Joules"/"Kelvins"];


PhotonEnergy[\[Lambda]_]:=\[HBar] c/\[Lambda]
CylinderCavity[waveguide_waveguide]:= 2\[Pi] (GetW[waveguide])^2*GetLength[waveguide]
PhotonElectricField[\[Lambda]_,waveguide_waveguide]:=Sqrt[UnitConvert[(2PhotonEnergy[\[Lambda]])/(\[Epsilon]0 CylinderCavity[waveguide]), ("Volts"/"Micrometers")^2]]
RabiFrequency[\[Lambda]_,waveguide_waveguide]:=UnitConvert[ ((ElectricDipoleMoment/Sqrt[3])PhotonElectricField[\[Lambda], waveguide])/\[HBar], "Hertz"]


FermiDiracDist[\[Omega]_, T_]:=1/E^((\[HBar] \[Omega])/(kb T)-1);
CasimirPolder[z_, \[Omega]_, T_]:=UnitConvert[ElectricDipoleMoment^2/(8\[Pi]*\[Epsilon]0*z^4) (FermiDiracDist[\[Omega],T]- (kb T)/(\[HBar] \[Omega])),"Newtons"];


End[];
EndPackage[];

